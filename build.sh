#!/bin/sh
TAG=br1.25.0-20210330;
docker buildx build --no-cache --pull --tag registry.gitlab.com/andrielson/fusionauth:$TAG .;
docker image tag registry.gitlab.com/andrielson/fusionauth:$TAG registry.gitlab.com/andrielson/fusionauth:latest;
